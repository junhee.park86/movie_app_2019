import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import axe from 'react-axe';

// eslint-disable-next-line no-undef
if (process.env.NODE_ENV !== 'production') {
  axe(React, ReactDOM, 1000);
} 

ReactDOM.render(<App />, document.getElementById('potato'));